import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class WalletTest {
    @Test
    void getBalance() {
        Wallet wallet = new Wallet();
        assertThat(wallet.getBalance()).isEqualTo(BigDecimal.ZERO);
    }

    @Test
    void fundWallet() {
        Wallet wallet = new Wallet();
        wallet.fund(50.0);

        assertThat(wallet.getBalance()).isEqualTo(valueOf(50.0));

        wallet.fund(10.0);
        assertThat(wallet.getBalance()).isEqualTo(valueOf(60.0));
    }

    @Test
    void withdraw() {
        Wallet wallet = new Wallet();
        wallet.fund(40.0);
        wallet.withdraw(10.0);

        assertThat(wallet.getBalance()).isEqualTo(valueOf(30.00));

        wallet.withdraw(28.10);
        assertThat(wallet.getBalance()).isEqualTo(valueOf(1.90));
    }

    @Test
    void withdraw_whenAmountGreaterThanBalance() {
        Wallet wallet = new Wallet();
        wallet.fund(20.0);
        assertThatThrownBy(() -> wallet.withdraw(50.0))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Insufficient funds");
    }

    private static class Wallet {
        private BigDecimal balance = BigDecimal.ZERO;

        public BigDecimal getBalance() {
            return balance;
        }

        public void fund(Double v) {
            balance = balance.add(valueOf(v));
        }

        public void withdraw(Double v) {
            BigDecimal amount = valueOf(v);
            ensureWithdrawalAmountIsValid(amount);
            balance = balance.subtract(amount);
        }

        private void ensureWithdrawalAmountIsValid(BigDecimal amount) {
            if (balance.compareTo(amount) < 0) {
                throw new RuntimeException("Insufficient funds");
            }
        }
    }
}

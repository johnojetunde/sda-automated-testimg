

import org.junit.jupiter.api.*;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        //given
        calculator = new Calculator();
    }

    @Test
    public void add() {
        //when
        Integer firstAddition = calculator.add(3);

        //then
        assertEquals(3, firstAddition);
    }

    @Test
    void sub() {
        //when
        Integer addition = calculator.add(50);

        Integer finalValue = calculator.sub(30);

        //then
        assertEquals(20, finalValue);
    }

    @Test
    public void divide() {

        assertThatThrownBy(() -> calculator.divide(5))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Unsupported division feature");
        //then
        //should throw an exception

    }

    @Test
    void attendanceCheck() {
        String[] students =
                List.of("NicoD", "Adrian", "Junn", "Jim", "Danica", "Jasher", "Json", "Roylan", "Jaime", "Wilfredo")
                        .toArray(new String[10]);

        List<String> studentsPresent =
                List.of("Jim", "NicoD", "Adrian", "Wilfredo", "Junn", "Danica", "Jasher", "Json", "Roylan", "Jaime");

        assertThat(studentsPresent).containsExactlyInAnyOrder(students);
    }

    static class Calculator {

        private Integer value = 0;

        public Integer add(Integer newValue) {
            value += newValue;
            return value;
        }

        public Integer sub(Integer newValue) {
            value -= newValue;
            return value;
        }

        public Integer divide(Integer newValue) {
            throw new RuntimeException("Unsupported division feature");
        }
    }
}


